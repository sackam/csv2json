<?php
/**
 * Small view class.
 *
 * Reference Zend framework.
 *
 * @author Daisuke Sakata <sakatad@websegment.net>
 */
class View{
	private $id;
	private static $instance;
	public function __construct(){}
	public static function getInstance(){
		if (!isset(self::$instance)) {
			self::$instance = new View;
		}
		return self::$instance;
	}
	public function getId(){
		return $this->id;
	}
	/**
	 * @see self::h;
	 * @param string $string
	 * @return string
	 */
	public function e($string){
		return self::h($string);
	}
	/**
	 * @see self::h;
	 * @param string $string
	 * @return string
	 */
	public function escape($string){
		return self::h($string);
	}
	/**
	 * @param string $string
	 * @return string
	 */
	public function h($string){
		return htmlspecialchars($string);
	}
	/**
	 * Set attributes.
	 *
	 * @param string $name
	 * @param array $options
	 * @return array
	 */
	private function _setAttribute($name, $attributes){
		$attr = array();
		$attr[0] = ' id="'.self::h($name).'"';
		if(!is_null($attributes)){
			foreach($attributes as $key => $value){
				if($key === 'id'){
					$attr[0] = sprintf(' %s="%s"', self::h($key), self::h($value));
				}else{
					$attr[] = sprintf(' %s="%s"', self::h($key), self::h($value));
				}
			}
		}
		return $attr;
	}
	/**
	 * Generate input form text element.
	 *
	 * @param string $name
	 * @param string $defaultValue
	 * @param array $options
	 * @return string
	 */
	public function text($name, $defaultValue = '', $attributes = array()){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<input type="text" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
	}
	/**
	 * Generate input form text-tel element.
	 *
	 * @param string $name
	 * @param string $defaultValue
	 * @param array $options
	 * @return string
	 */
	public function tel($name, $defaultValue = '', $attributes = array()){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<input type="tel" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
	}
	public function search($name, $defaultValue = '', $attributes = array()){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<input type="search" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
	}
	public function url($name, $defaultValue = '', $attributes = array()){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<input type="url" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
	}
	public function password($name, $defaultValue = '', $attributes = array()){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<input type="password" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
	}
	public function datetime($name, $defaultValue = '', $attributes = array()){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<input type="datetime" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
	}
	public function date($name, $defaultValue = '', $attributes = array()){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<input type="date" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
	}
	public function month($name, $defaultValue = '', $attributes = array()){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<input type="month" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
	}
	public function week($name, $defaultValue = '', $attributes = array()){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<input type="week" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
	}
	public function datetimeLocal($name, $defaultValue = '', $attributes = array()){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<input type="datetime-local" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
	}
	public function number($name, $defaultValue = '', $attributes = array()){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<input type="numbertime" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
	}
	/**
	 * Generate input form text-email element.
	 *
	 * @param string $name
	 * @param string $defaultValue
	 * @param array $options
	 * @return string
	 */
	public function email($name, $defaultValue = '', $attributes = array()){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<input type="email" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
	}
	/**
	 * Generate input form select element.
	 *
	 * @param string $name
	 * @param string $defaultValue
	 * @param array $attributes
	 * @param array $options
	 * @return string
	 */
	public function select($name, $defaultValue = '', $attributes = null, $options = array(), $blankSpace = null){
		$attr = self::_setAttribute($name, $attributes);
		$tag[] = sprintf('<select name="%s"%s>', self::h($name), implode('', $attr));
		if($blankSpace !== null){
			$tag[] = sprintf('<option value="" label="">%s</option>',
				$blankSpace);
		}
		foreach($options as $value => $label){
			$selected = '';
			if(is_array($label)){
				$tag[] = sprintf('<optgroup label="%s">', self::h($value));
				foreach ($label as $moreValue => $moreLabel) {
					$selected = '';
					if($defaultValue == $moreValue){
						$selected = ' selected="selected"';
					}
					$tag[]= sprintf('<option value="%s" label="%s"%s>%s</option>',
						self::h($moreValue), self::h($moreLabel), $selected, self::h($moreLabel));
				}
				$tag[] = '</optgroup>';
			}else{
				if($defaultValue == $value){
					$selected = ' selected="selected"';
				}
				$tag[] = sprintf('<option value="%s" label="%s"%s>%s</option>',
					self::h($value), self::h($label), $selected, self::h($label));
			}
		}
		$tag[] = '</select>';
		return implode(PHP_EOL, $tag);
	}
	/**
	 * Generate textarea form element.
	 *
	 * @param string $name
	 * @param string $defaultValue
	 * @param array $options
	 * @return string
	 */
	public function textarea($name, $defaultValue, $attributes = null){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<textarea name="%s"%s>%s</textarea>',
			self::h($name), implode('', $attr), self::h($defaultValue));
	}
	/**
	 * Generate input form hidden element.
	 *
	 * @param string $name
	 * @param string $defaultValue
	 * @param array $options
	 * @return string
	 */
	public function hidden($name, $defaultValue, $attributes = null){
		$attr = self::_setAttribute($name, $attributes);
		return sprintf('<input type="hidden" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
	}
	/**
	 * Generate input form checkbox element.
	 *
	 * @param string $name
	 * @param string $defaultValue
	 * @param array $options
	 * @return string
	 */
	public function checkboxes($name, $defaultValue = array(), $attributes = null, $options = null, $delimiter = '<br>'){
		$counter = 0;
		$checkboxes = array();
		$id = $name . '-' . $counter;
		$attr = self::_setAttribute($name, $attributes);
		$tag = self::hidden($name, 0, array('id' => $id));
		if($options != null){
			foreach ($options as $value => $label) {
				$counter++;
				$id = $name . '-' . $counter;
				$attributes['id'] = $id;
				if(is_array($defaultValue) && in_array($value, $defaultValue)){
					$attributes['checked'] = 'checked';
					$attr = self::_setAttribute($name, $attributes);
				}else{
					if(isset($attributes['checked'])){
						unset($attributes['checked']);
					}
					$attr = self::_setAttribute($name, $attributes);
				}
				$checkboxes[] = sprintf('<label for="%s"><input type="checkbox" name="%s" value="%s"%s>%s</label>',
					$id, self::h($name.'[]'), self::h($value), implode('', $attr), self::h($label));
			}
			$tag .= implode($delimiter . PHP_EOL, $checkboxes);
		}else{
			$tag .= sprintf('<input type="checkbox" name="%s" value="%s"%s>',
				self::h($name), self::h($defaultValue), implode('', $attr));
		}
		return $tag;
	}
	/**
	 * Generate input radio element.
	 *
	 * @param string $name
	 * @param string $defaultValue
	 * @param array $options
	 * @return string
	 */
	public function formRadio($name, $defaultValue, $attributes = null, $options = array()){
		$attr = self::_setAttribute($name, $attributes);
		$tag = self::hidden($name, $defaultValue);
		$tag .= sprintf('<input type="radio" name="%s" value="%s"%s>',
			self::h($name), self::h($defaultValue), implode('', $attr));
		return $tag;
	}
	/**
	 * Generate input radio list elements.
	 *
	 * @param string $name
	 * @param string $defaultValue
	 * @param array $options
	 * @param string $delimiter
	 * @return string
	 */
	public function formMultiRadio($name, $defaultValue, $attributes = null, $options = null, $delimiter = '<br>'){
		$counter = 0;
		$radios = array();
		$attr = self::_setAttribute($name, $attributes);
		$tag = self::hidden($name, 0, array('id' => $name . '-' . $counter));
		if($options != null){
			foreach($options as $value => $label){
				$counter++;
				$id = $name . '-' . $counter;
				$attributes['id'] = $id;
				if($value == $defaultValue){
					$attributes['checked'] = 'checked';
					$attr = self::_setAttribute($name, $attributes);
				}else{
					if(isset($attributes['checked'])){
						unset($attributes['checked']);
					}
					$attr = self::_setAttribute($name, $attributes);
				}
				$radios[] = sprintf('<label for="%s"><input type="radio" name="%s" value="%s"%s>%s</label>',
					$id, self::h($name), self::h($value), implode('', $attr), self::h($label), $delimiter);
			}
			$tag .= implode($delimiter . PHP_EOL, $radios);
		}else{
			$tag .= sprintf('<input type="radio" name="%s" value="%s"%s>',
				self::h($name), self::h($defaultValue), implode('', $attr));
		}
		return $tag;
	}
}
class ViewException extends Exception{
}

<?php
/**
 * Request class.
 * 
 * @author Daisuke Sakata <sakatad@websegment.net>
 */
class Request{
	private $id;
	private static $instance;
	public function __construct(){}
	public static function getInstance(){
		if (!isset(self::$instance)) {
			self::$instance = new Request;
		}
		return self::$instance;
	}
	public function getId(){
		return $this->id;
	}
	/**
	 * Get post data.
	 * 
	 * @access public
	 * @param  string $name
	 * @param  string $defaultValue
	 * @return mixed  String:paramter, Array:all parameters
	 */
	public function getPost($name = null, $defaultValue = ''){
		if($name === null){
			return $_POST;
		}else if(isset($_POST[$name])){
			return $_POST[$name];
		}else{
			return $defaultValue;
		}
	}
	/**
	 * Get query string data.
	 * 
	 * @access public
	 * @param  string $name
	 * @param  string $defaultValue
	 * @return mixed  String:paramter, Array:all parameters
	 */
	public function getQuery($name = null, $defaultValue = ''){
		if($name === null){
			return $_GET;
		}else if(isset($_GET[$name])){
			return $_GET[$name];
		}else{
			return $defaultValue;
		}
	}
	/**
	 * Get cookie data.
	 * 
	 * @access public
	 * @param  string $name
	 * @param  string $defaultValue
	 * @return mixed  String:parameter, Array:all parameters
	 */
	public function getCookie($name = null, $defaultValue = ''){
		if($name === null){
			return $_COOKIE;
		}else if(isset($_COOKIE[$name])){
			return $_COOKIE[$name];
		}else{
			return $defaultValue;
		}
	}
	/**
	 * Get session data.
	 * 
	 * @access public
	 * @param string $name
	 * @param string $defaultValue
	 * @return mixed String:parameter, Array:all parameters
	 */
	public function getSession($name = null, $defaultValue = ''){
		if($name === null){
			return $_SESSION;
		}else if(isset($_SESSION[$name])){
			return $_SESSION[$name];
		}else{
			return $defaultValue;
		}
	}
	public function setSession($name, $value = ''){
		$_SESSION[$name] = $value;
	}
	public function isPost(){
		return ($_SERVER['REQUEST_METHOD'] == 'POST') ? true : false;
	}
	public function isGet(){
		return ($_SERVER['REQUEST_METHOD'] == 'GET') ? true : false;
	}
}
class RequestException extends Exception{}
<?php
setlocale(LC_ALL, 'ja_JP');
session_start();

require_once dirname(__FILE__) . '/Request.php';
require_once dirname(__FILE__) . '/View.php';
require_once dirname(__FILE__) . '/CSRFValidate.php';

define('KB', 1024);

$defaultEd = 'id	title	content	created_at	updated_at
1	タイトル	コンテンツ	2017/2/18	2017/2/18';

$defaultMode = array(
	JSON_UNESCAPED_UNICODE,
	JSON_PRETTY_PRINT,
	);
$defaultDelimiter = 't';

$json = array();
$jsonLine = array();
$encodeJson = '';
$summary = 0;

$delimiters = array(
	't' => 'タブ区切り',
	',' => 'カンマ区切り',
	);
$mode = array(
	JSON_HEX_TAG => 'すべての < > をそれぞれ \u003C および \u003E に変換',
	JSON_HEX_AMP => 'すべての & を \u0026 に変換',
	JSON_HEX_APOS => 'すべての \' を \u0027 に変換',
	JSON_HEX_QUOT => 'すべての " を \u0022 に変換',
	JSON_FORCE_OBJECT => '配列ではなくオブジェクトで出力',
	JSON_NUMERIC_CHECK => '数字形式を数値形式でエンコード',
	JSON_BIGINT_AS_STRING => '大きな整数値を、文字列型でエンコード',
	JSON_UNESCAPED_SLASHES => '/をエスケープしない',
	JSON_PRETTY_PRINT => '返される結果の書式を、スペースを使って整形',
	JSON_UNESCAPED_UNICODE => 'マルチバイト Unicode 文字をそのままの形式で',
	JSON_PARTIAL_OUTPUT_ON_ERROR => 'エンコード不可能な値は代替値に置き換え',
	JSON_PRESERVE_ZERO_FRACTION => 'float 型の値を常に float 値としてエンコード',
	);


$req = Request::getInstance();
$view = new View();

if($req->isPost()){
	if (!CSRFValidate::validate(filter_input(INPUT_POST, 'token'))) {
		header('Content-Type: text/plain; charset=UTF-8', true, 400);
		die('CSRF validation failed.');
	}
	$fileName = 'tmp/' . md5(microtime());
	$delimiter = $req->getPost('del');
	if($req->getPost('del') == 't'){
		$delimiter = "\t";
	}

	$fp = fopen($fileName, 'w');
	fputs($fp, $req->getPost('ed'));
	fclose($fp);

	$fp = fopen($fileName, 'r');
	$header = fgetcsv($fp, KB, $delimiter);
	while($line = fgetcsv($fp, KB, $delimiter)){
		foreach($header as $key => $head){
			$jsonLine[$head] = $line[$key];
		}
		$json[] = $jsonLine;
	}
	fclose($fp);
	unlink($fileName);
	if($req->getPost('mode') != '0'){
		foreach($req->getPost('mode') as $value){
			$summary += $value;
		}
	}
	$encodeJson = json_encode($json, $summary);
}

function e($str){
	return htmlspecialchars($str);
}
<?php
require_once 'lib/main.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>エクセルデータからJSONデータを作成</title>
	<link rel="stylesheet" href="/c2j/css/c2j.css">
</head>
<body>
<div class="container">
	<header>
		<h1>エクセルデータからJSONデータを作成</h1>
	</header>
	<article>
		<h1>CSV to JSON</h1>
		<p>エクセルのデータを範囲選択し、そのままテキストエリアに貼り付けてください。</p>
		<form id="form" action="" method="post">
			<div class="form-group">
<?php echo $view->textarea('ed', $req->getPost('ed', $defaultEd), array('class' => 'form-control', 'rows' => 10))?>

			<p class="help-block">※1つのセル内に改行が有る場合はダブルクォーテーションで囲って下さい。</p>
			</div>
			<div class="form-group">
<div class="radio-inline"><?php echo $view->formMultiRadio('del', $req->getPost('del', $defaultDelimiter), null, $delimiters, '</div><div class="radio-inline">')?></div>

			</div>
			<div class="form-group">
				<h3>JSON options</h3>
<div class="checkbox"><?php echo $view->checkboxes('mode', $req->getPost('mode', $defaultMode), null, $mode, '</div><div class="checkbox">')?></div>

			</div>
			<div class="form-group">
				<button id="submit" type="submit" name="commit" class="btn btn-primary btn-lg">変換</button>
			</div>
		<input type="hidden" name="token" value="<?= CSRFValidate::generate()?>"></form>
<?php if(count($json) > 0):?>
		<hr>
		<h2>Converted JSON</h2>
<?php echo $view->textarea('json', $encodeJson, array('class' => 'form-control', 'rows' => 10))?>

		<h2>Converted CSV</h2>
		<div class="table-responsive">
		<table class="table">
			<tr>
<?php foreach($header as $head):?>
				<th><?= $head?></th>
<?php endforeach;?>
			</tr>
<?php foreach($json as $data):?>
			<tr>
<?php 		foreach($data as $d):?>
				<td><?= nl2br(e($d))?></td>
<?php 		endforeach;?>
			</tr>
<?php endforeach;?>
		</table>
		</div>
<?php endif;?>
	</article>
	<footer>
		<small>&copy; 2017 Sackam</small>
	</footer>
</div>
<script src="//code.jquery.com/jquery-1.12.1.min.js"></script>
<script>window.jQuery || document.write("<script src='\/c2j\/js\/jquery\-1.12.1.min.js'><\/script>")</script>
<script src="/c2j/js/bootstrap.min.js"></script>
</body>
</html>
